var gulp = require('gulp');
var replace = require('gulp-replace');
var vulcanize = require('gulp-vulcanize');
var crisper = require('gulp-crisper');
var connect = require('gulp-connect');
var shell = require('gulp-shell');
var minifyInline = require('gulp-minify-inline');

gulp.task('build', function() {
    gulp.src('temp/E310F01C-A85B-4853-AE46-F9CF89B278C9/*')
        .pipe(gulp.dest('dist/E310F01C-A85B-4853-AE46-F9CF89B278C9'));

    return gulp.src('campus-individualized-education-program.html')
        .pipe(replace(/<link rel="import" href="../g, '<link rel="import" href="../../bower_components'))
        .pipe(gulp.dest('dist/E310F01C-A85B-4853-AE46-F9CF89B278C9'))
        .pipe(replace(/<script src="../g, '<script src="../../bower_components'))
        .pipe(gulp.dest('dist/E310F01C-A85B-4853-AE46-F9CF89B278C9'))
        .pipe(replace(/campus-behavior.html/g, 'campus-develope.html'))
        .pipe(gulp.dest('dist/E310F01C-A85B-4853-AE46-F9CF89B278C9'))
        .pipe(vulcanize({
            inlineScripts: true,
            inlineCss: true,
            stripComments: true
        }))
        .pipe(minifyInline())
        .pipe(crisper())
        .pipe(gulp.dest('dist/E310F01C-A85B-4853-AE46-F9CF89B278C9'));
});

gulp.task('serve', function() {
    connect.server({
        root: 'dist'
    });
});

gulp.task('default', ['serve', 'build'], shell.task([
    /^win/.test(require('os').platform()) ? 'start http://localhost:8080/' : 'open http://localhost:8080/'
]));
